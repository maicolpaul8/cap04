//import { useState } from 'react'
//import logo from './logo.svg'
import './App.css'
import Checkbox from './Menu/checkbox';
import check from './images/1398913_circle_correct_mark_success_tick_icon.png';

import styled from 'styled-components';

import Options,{params} from './Menu/option';

const Container=styled.div`
  background-color:red;
`
// function App() {
//   const [count, setCount] = useState(0)

//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>Hello Vite + React!</p>
//         <p>
//           <button type="button" onClick={() => setCount((count) => count + 1)}>
//             count is: {count}
//           </button>
//         </p>
//         <p>
//           Edit <code>App.tsx</code> and save to test HMR updates.
//         </p>
//         <p>
//           <a
//             className="App-link"
//             href="https://reactjs.org"
//             target="_blank"
//             rel="noopener noreferrer"
//           >
//             Learn React
//           </a>
//           {' | '}
//           <a
//             className="App-link"
//             href="https://vitejs.dev/guide/features.html"
//             target="_blank"
//             rel="noopener noreferrer"
//           >
//             Vite Docs
//           </a>
//         </p>
//       </header>
//     </div>
//   )
// }
const App=()=>{
  const options:string[]=[    
        "lup nutrition",
        "asitis",        
        "avvatar",    
        "big muscles",
        "bpi sports",
        "bsn",
        "cellucor",      
        "domin"      
  ];

  const handleClick:params['onClick']=(e)=>{
    console.log(e);
  };

  return (<Container>
    {/* <Checkbox image={check} onClick={e=>console.log(e)}></Checkbox> */}
    {
      options.map((v,i)=><Options key={i} value={v} onClick={handleClick}></Options>)
    }
    </Container>);
};

export default App;
