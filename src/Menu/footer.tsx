import  styled  from "styled-components";

import Checkbox from './checkbox';

import check from '../images/1398913_circle_correct_mark_success_tick_icon.png';

import cancel from '../images/1398919_close_cross_incorrect_invalid_x_icon.png';

import reset from '../images/172536_reload_icon.png';

import { useEffect, useState } from 'react'

const Container=styled.div`    
background-color:${props=>props.color};
justify-content:center;
flex-direction:row;
display:flex;  
`;

const ContainerV1=styled.div`    
background-color:${props=>props.color};
justify-content:center;
flex-direction:row;
display:flex;
margin-top:15px;
padding-top:25px;
padding-bottom:25px;    
border-top:1px solid #ffffff80;
`;

interface option{
    icon:string;
    onClick:()=>void;
};

export interface params{
    enableReset:boolean;
    enableCancel:boolean;    
};

const App=(params:params)=>{

    // const [options,setOptions]=useState<options[]>([
    //     {icon:check,onClick:()=>{}}
    // ]);

    // useEffect(()=>{
    //     // {icon:cancel,onClick:handleCancel},
    //     // {icon:reset,onClick:handleReset},
    //     // {icon:check,onClick:handleOk}
    // },[]);

    const handleCancel=()=>{
        console.log("vamos a resetear");
        if (typeof onReset==='function') onCancel()
    };

    const handleReset=()=>{
        console.log("vamos a resetear");
        if (typeof onReset==='function') onAcept()
    };

    const handleOK=()=>{
        console.log("vamos a resetear");
        if (typeof onReset==='function') onReset()
    };

    const data:option[]=[
        {icon:cancel,onClick:handleCancel},
        {icon:reset,onClick:handleReset},
        {icon:check,onClick:handleOK}
    ];

    return (
            <Container>
             {data.map((v,i)=><Checkbox state={false} onClick={v.onClick}></Checkbox>)}       
            </Container>);
  };
  
  export default App;