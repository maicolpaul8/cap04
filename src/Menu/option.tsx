
import  styled  from "styled-components";

import check from '../images/1398913_circle_correct_mark_success_tick_icon.png';

import Checkbox from './checkbox';

const Container=styled.div`
    margin-left:25px;
    margin-top:15px;
    display:flex;
    flex:direction:row;
    justify-content:space-between;
    margin-right:25px;
`;

const Title=styled.p`
    margin:0px;
    color:white;
    font-weight:500;
    text-transform:uppercase;
`;
export interface params{    
    value:string;    
    onClick?:(params:{value:string,state:boolean})=>void;
};

const App=(params:params)=>{

    const handleClick=(state:boolean)=>{
        if (typeof params.onClick==='function') params.onClick({value:params.value,state});
    }

    return (
        <Container>
            <Title>
                {params.value}
            </Title>
                <Checkbox image={check} onClick={handleClick}>
                    
                </Checkbox>
        </Container>);
};

export default App;