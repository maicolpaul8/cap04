import { useState } from "react";
import  styled  from "styled-components";
//import check from '../images/1398913_circle_correct_mark_success_tick_icon.png';

const Container=styled.div<{visible:boolean}>`
border-radius:50%;
border:1.8px solid white;
height:35px;
width:35px;
cursor:pointer;
background-color:${props=>props.visible ? 'white' : 'transparent'}
`;

const Icon=styled.img<{visible:boolean}>`
height:35px;
width:35px;
object-fit:contain;
visibility:${props=>props.visible ? 'visible' : 'hidden'}
`;

export interface params{       
    image:string; 
    //state:boolean;
    onClick?:(value:boolean)=>void;
};

const App=(params:params)=>{

    const [visible,setVisible]=useState<boolean>(false);

    const handleClick=()=>{
        setVisible(!visible);
        if (typeof params.onClick==='function') params.onClick(!visible);
    }
    return (
        <Container visible={visible} onClick={handleClick}>
            <Icon src={params.image} visible={visible}></Icon>
        </Container>);
};

export default App;